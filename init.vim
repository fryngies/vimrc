if(!filereadable(expand("~/.local/share/nvim/site/autoload/plug.vim")))
	echo 'plug.vim not found, trying to install...'
	call system(expand("curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"))
endif

"-- General --
set nocompatible
set number		"Show line numbers
set relativenumber
set ruler		"Show the line and column number of the cursor position
set mouse=a
set cursorline

call plug#begin()
" colorscheme, syntax highlighting
Plug 'frankier/neovim-colors-solarized-truecolor-only'
Plug 'rust-lang/rust.vim'

" git helpers
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

" utils
Plug 'vim-airline/vim-airline'
Plug 'neomake/neomake'
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'jiangmiao/auto-pairs'
Plug 'timonv/vim-cargo'
Plug 'alvan/vim-closetag'
Plug 'tpope/vim-surround'
Plug 'tomtom/tcomment_vim'
Plug 'Chiel92/vim-autoformat'
Plug 'ryanoasis/vim-devicons'
Plug 'mattn/webapi-vim'
Plug 'mattn/gist-vim'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'ternjs/tern_for_vim', { 'do': 'npm install' }
Plug 'carlitux/deoplete-ternjs'
Plug 'ludovicchabant/vim-gutentags'

" CSS plugins
Plug 'JulesWang/css.vim'
Plug 'ap/vim-css-color'

call plug#end()

let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_ignore_case = 1
let g:deoplete#enable_smart_case = 1
let g:deoplete#enable_camel_case = 1
let g:deoplete#enable_refresh_always = 1
let g:deoplete#max_abbr_width = 0
let g:deoplete#max_menu_width = 0
let g:deoplete#omni#input_patterns = get(g:,'deoplete#omni#input_patterns',{})

let g:tern_request_timeout = 1
let g:tern_request_timeout = 6000
let g:tern#command = ["tern"]
let g:tern#arguments = ["--persistent"]

call deoplete#enable()
" omnifuncs
augroup omnifuncs
	autocmd!
	autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
	autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
	autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
	autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
	autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
augroup end
" tern
if exists('g:plugs["tern_for_vim"]')
	let g:tern_show_argument_hints = 'on_hold'
	let g:tern_show_signature_in_pum = 1
	autocmd FileType javascript setlocal omnifunc=tern#Complete
endif

" Neovim true color
set termguicolors
set guicursor=n-v-c:block-Cursor/lCursor-blinkon0,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor

set termguicolors
set background=dark
colorscheme solarized

"-- Programming --
set autoindent		"Copy indent from current line when starting a new line
syntax on
" Remember cursor position between vim sessions
autocmd BufReadPost *
	\ if line("'\"") > 0 && line ("'\"") <= line("$") |
	\   exe "normal! g'\"" |
	\ endif
	" center buffer around cursor when opening files
autocmd BufRead * normal zz

"-- Spaces/Tabs --
set noexpandtab		"Strictly use tabs when tab is pressed (this is the default)
set shiftwidth=4
set tabstop=4

"-- Searching --
set hlsearch		"Highlight search results
set ignorecase		"When doing a search, ignore the case of letters
set smartcase		"Override the ignorecase option if the search pattern contains upper case letters

"-- Tweaks --
set wildmenu
"Add tweak for better backspace support
set backspace=indent,eol,start
" clipboard
set clipboard=unnamed
set undofile
set undodir="$HOME/.VIM_UNDO_FILES"
" use powerline fonts
let g:airline_powerline_fonts=1
" Youcompleteme fix (kinda)
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
"autopen NERDTree and focus cursor in new document
" autocmd VimEnter * NERDTree
" autocmd VimEnter * wincmd p
map <C-\> :NERDTreeToggle<CR>:wincmd p<CR>
" close vim if the only window left open is a NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" NERDTree minimal UI
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

" Gist
if has('mac')
	let g:gist_clip_command = 'pbcopy'
elseif has('linux')
	let g:gist_clip_command = 'xclip -selection clipboard'
endif
let g:gist_detect_filetype = 1
let g:gist_post_private = 1

" autorun neomake on the current file write
autocmd! BufWritePost * Neomake
" some clang args
let g:neomake_cpp_clang_args = ["-std=c++14", "-Wextra", "-Wall", "-fsanitize=undefined","-g"]

let g:neomake_javascript_enabled_makers = ['eslint']

" Hotkeys
map <Enter> o<ESC>
map <S-Enter> O<ESC>

" clear the search highlight by pressing ESC when in Normal mode (Typing commands)
map <esc> :noh<cr>
noremap j gj
noremap k gk

"-- Tabbed Editing --
"Open a new (empty) tab by pressing CTRL-T. Prompts for name of file to edit
map <C-T> :tabnew<CR>:edit 
"Open a file in a new tab by pressing CTRL-O. Prompts for name of file to edit
map <C-O> :tabfind 
"Switch between tabs by pressing Shift-Tab
map <S-Tab> gt


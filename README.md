My Neovim config.

Depends on:

- [`universal-ctags`](https://github.com/universal-ctags/ctags/)
- `neovim` from `pip`
- [`ternjs`](https://github.com/ternjs/tern) (globally)
- `curl`